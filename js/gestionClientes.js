/*
function gestionClientes(){
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var request = new XMLHttpRequest();
//Definir datos de mi petición
  request.onreadystatechange = function()
  //cuando el objeto request haya cambiado su estado, se ejecute el sigueiente código
  {
    if(this.readyState == 4 && this.status == 200)
    {
      console.log(request.responseText);
    }
  }
  request.open("GET",url,true);
  request.send();
}
*/

var clientesObtenidos;

function gestionClientes(){
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
//Definir datos de mi petición
  request.onreadystatechange = function()
  //cuando el objeto request haya cambiado su estado, se ejecute el sigueiente código
  {
    if(this.readyState == 4 && this.status == 200)
    {
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes()
{
//
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById("tablaClientes");

  for(var i=0; i< JSONClientes.value.length; i++ )
  {
      var nuevaFila = document.createElement("tr");

      var columnaCustomerID = document.createElement("td");
      columnaCustomerID.innerText = JSONClientes.value[i].CustomerID;


      var columnaCompanyName = document.createElement("td");
      columnaCompanyName.innerText = JSONClientes.value[i].CompanyName;  


      var columnaCountry = document.createElement("td");
      columnaCountry.innerText = JSONClientes.value[i].Country;


      var columnaBandera = document.createElement("img");
      columnaBandera.classList.add("flag");
      //alert(JSONClientes.value[i].Country);
      if(JSONClientes.value[i].Country=="UK"){

        columnaBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of"+"-United-Kingdom.png";
      }else{
        //alert("NO Soy UK");
        columnaBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png";
      }
      nuevaFila.appendChild(columnaCustomerID);
      nuevaFila.appendChild(columnaCompanyName);
      nuevaFila.appendChild(columnaCountry);
      nuevaFila.appendChild(columnaBandera);
      tabla.appendChild(nuevaFila);

      //alert(JSONClientes.value[i].ProductName);
  }
  //alert(JSONClientes.value[0].ProductName);

}
